# Simple Yu-Gi-Oh! Deck Browser
This project is build with Angular version 6.1.1.

## Prerequisites
  * Node.js - Download and install Node.js

## Stack
  * Angular 6
  * Bootstrap 4
  * Angular Perfect Scrollbar `ngx-perfect-scrollbar`

## Getting Started

1. Clone or download repository
2. Install the dependencies
``npm install``
3. Start the application
```ng serve```
4. Navigate to `http://localhost:4200/`

## Running unit tests

Run `ng test` to execute the unit tests via Karma.

## Development Process
Firstly two components were created, CardsComponent and CardDetailsComponent. Inside the CardsComponent a mock-card was created, that contains an array of the names of all the card. CardsComponent handles the cards included in the mock-data file. CardDetailsComponent is the one that displays the selected card.  
Secondly, all the templates were created according to BEM naming methodology and using Bootstrap and Perfect Scrollbar.  
Then, basic functionality was created (routing, getting JSON data from the server, error handling).  
Finally, this project implements very basic unit testing, no extended unit test is added yet. 

### Author
  * Florentia Pantazi
  * GitHub: `https://github.com/laurapantazi`