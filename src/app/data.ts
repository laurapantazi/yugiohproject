export interface Data {
	status: string;
	data: {
		name: string,
		text: string,
		card_type?: string,
		family?: string,
		atk?: number,
		def?: number,
		level?: number,
		property?: string
	}
}