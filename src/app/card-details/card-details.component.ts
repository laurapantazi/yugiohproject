import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { CardDataService } from '../card-data.service';
import { Data } from '../data';

@Component({
  selector: 'app-card-details',
  templateUrl: './card-details.component.html',
  styleUrls: ['./card-details.component.css']
})
export class CardDetailsComponent implements OnInit {
	image: string;
	name: string;
	fullData = {};
  error = {};
  constructor(
	private route: ActivatedRoute,
  private router: Router,
  private dataService: CardDataService
  ) { }

  ngOnInit() {
  	this.getCard();
  }

  getCard(){
  	this.name = this.route.snapshot.paramMap.get("name");
  	 	if(this.name){
		  	this.dataService.getData(this.name).subscribe((data: Data)=>
		  	{
		  		if(data['status']=="success")
		  		{
		  			this.fullData = data['data'];
		  		}
            this.image = `http://52.57.88.137/api/card_image/${this.name}`;
		  	}, 
        error => this.error = error
        );
  		}
  }
}
