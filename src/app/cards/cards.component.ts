import { Component, OnInit } from '@angular/core';
import { CARDS } from './mock-cards';
import { CardDataService } from '../card-data.service';
import { Data } from '../data';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {
	error = {};
	type: string;
	cards = CARDS;
	cardType = new Map();
	constructor(private dataService: CardDataService) { }

  ngOnInit() {
  	this.showTypes();
  }
  showTypes(){
	  	for (let item of this.cards){
	  		this.dataService.getType(item).subscribe((data: Data)=>
		  	{
		  		this.type = data['data']['card_type'];
		  		this.cardType.set(item,this.type);
			},
			error => this.error = error
			);	
		}
  	}
}
