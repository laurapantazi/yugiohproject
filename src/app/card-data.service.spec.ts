import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule, HttpErrorResponse } from '@angular/common/http';

import { CardDataService } from './card-data.service';

describe('CardDataService', () => {

	let cardDataService: CardDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
			imports: [
      HttpClientModule
      ],
      providers: [CardDataService]
    });
  });

  it('should be created', inject([CardDataService], (service: CardDataService) => {
    expect(service).toBeTruthy();
  }));

  describe('getCard()', () => {
	  it('should return name of a card ', () =>{
	  	inject([CardDataService], (cardDataService) =>{
	  		cardDataService.getData('http://52.57.88.137/api/card_data/Reasoning').subscribe((card)=>{
	  			card => expect(card.data.name).toEqual('Reasoning');
	  		})
	  	})
		});
	});

});