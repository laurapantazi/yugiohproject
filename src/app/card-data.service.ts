import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Data } from './data';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CardDataService {
	url: string;
  constructor(private http: HttpClient) { }

  getData(name){
  	this.url = encodeURI(`http://52.57.88.137/api/card_data/${name}`);
  	return this.http.get<Data>(this.url).
      pipe(catchError(this.handlerError)); 	
  }

  getType(name){
  	this.url = encodeURI(`http://52.57.88.137/api/card_data/${name}`);
  	return this.http.get<Data>(this.url).
      pipe(catchError(this.handlerError));
  }

  handlerError(error: HttpErrorResponse){
    if(error.error instanceof ErrorEvent){
      console.error(`An error occured: ${error.error.message}`, error.error.message);
    }
    else{
      console.error(error.status,`Backend returned code ${error.status}, body was: `, error.error);
    }
    return throwError(error);
    
  }
}
